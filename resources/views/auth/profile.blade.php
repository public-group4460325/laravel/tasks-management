@extends('layouts.structures.strucutre') @section('content')

    <!-- BEGIN: Content-->
    <div class="content-right">
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="modal-content" style="margin-left: 10%; margin-top: 10%">
                    <div class="modal-body pb-5 px-sm-5 pt-50">
                        <div class="text-center mb-2">
                            <h1 class="mb-1">Edit Profile Information</h1>
                        </div>
                        <form id="editUserForm" class="row gy-1 pt-75" action="{{ route('profile.update') }}" method="POST">
                            @csrf @method('POST')
                            <div class="col-12">
                                <label class="form-label" for="modalEditUserName">Name</label>
                                <input type="text" id="modalEditUserName" name="name"
                                    class="form-control @error('name') is-invalid @enderror" value="{{ $user->name }}"
                                    placeholder="example" /> @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label class="form-label" for="modalEditUserName">Email</label>
                                <input type="text" id="modalEditUserName" name="email"
                                    class="form-control @error('email') is-invalid @enderror" value="{{ $user->email }}"
                                    placeholder="exmaple@gmail.com" /> @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12">
                                <label class="form-label" for="modalEditUserName">New Password</label>
                                <input type="text" id="modalEditUserName" name="password"
                                    class="form-control @error('password') is-invalid @enderror" placeholder="........" />
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-12 text-center mt-2 pt-50">
                                <input type="submit" value="Update" class="btn btn-primary me-1">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection

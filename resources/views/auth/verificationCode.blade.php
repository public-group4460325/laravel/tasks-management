@extends('layouts.components.auth.strucutre') @section('content')
    @include('layouts.auth.image', [
        'imagePath' => 'app-assets/images/illustration/two-steps-verification-illustration.svg',
    ])
    @include('layouts.auth.verification_code_form')
    <script>
        function goNext(elem) {
            if ($(elem).val().length == 0)
                return;
            id = elem.id.replace("code-box-", "");
            id = Number(id) + 1
            if (allInputsFilled()) {
                $("#checkCodeForm").submit();
            }
            $(`#code-box-${id}`).focus();
        }

        function allInputsFilled() {
            inputs = $(".numeral-mask");
            inputs = Object.values(inputs);
            let allFilled = true;
            inputs.forEach(element => {
                if (element.id && element.id.includes('code-box') && $(element).val().length == 0) {
                    console.log(element);
                    allFilled = false;
                    return false;
                }
            });
            return allFilled;
        }
    </script>
@endsection

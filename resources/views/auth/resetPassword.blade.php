@extends('layouts.components.auth.strucutre') @section('content')
    @include('layouts.auth.image', ['imagePath' => 'app-assets/images/pages/reset-password-v2.svg'])
    @include('layouts.auth.register_form')
@endsection
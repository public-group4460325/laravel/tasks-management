@extends('layouts.components.auth.strucutre') @section('content')
    @include('layouts.auth.image', ['imagePath' => 'app-assets/images/illustration/verify-email-illustration.svg'])
    @include('layouts.auth.email_form')
@endsection
@extends('layouts.components.auth.strucutre') @section('content')
    @include('layouts.auth.image', ['imagePath' => 'app-assets/images/pages/login-v2.svg'])
    @include('layouts.auth.login_form')
@endsection

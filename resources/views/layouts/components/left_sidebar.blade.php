<div class="sidebar-left">
    <div class="sidebar">
        <div class="sidebar-content todo-sidebar">
            <div class="todo-app-menu">
                @if(Route::currentRouteName() == 'tasks.index')
                <div class="add-task">
                    <button type="button" class="btn btn-primary w-100" data-bs-toggle="modal"
                        data-bs-target="#new-task-modal">
                        Add Task
                    </button>
                </div>
                @endif
                <div class="add-task" style="display: flex; justify-content: space-evenly">
                    @if ($tasks->previousPageUrl())
                        <a href="{{ $tasks->previousPageUrl() }}" class="btn btn-primary">Previous</a>
                    @else
                        <a class="btn btn-outline-secondary" disabled>Previous</a>
                        @endif @if ($tasks->hasMorePages())
                            <a href="{{ $tasks->nextPageUrl() }}" class="btn btn-primary">Next</a>
                        @else
                            <a class="btn btn-outline-secondary" disabled>Next</a>
                        @endif
                </div>
                <div class="sidebar-menu-list">
                    <div class="list-group list-group-labels">
                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                            <span class="bullet bullet-sm bullet-primary me-1"></span>To Do Last
                        </a>
                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                            <span class="bullet bullet-sm bullet-warning me-1"></span>Normal
                        </a>
                        <a href="#" class="list-group-item list-group-item-action d-flex align-items-center">
                            <span class="bullet bullet-sm bullet-danger me-1"></span>Important
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

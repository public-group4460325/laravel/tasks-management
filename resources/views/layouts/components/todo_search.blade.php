<!-- Todo search starts -->
<div class="app-fixed-search d-flex align-items-center">
    <div class="sidebar-toggle d-block d-lg-none ms-1">
        <i data-feather="menu" class="font-medium-5"></i>
    </div>
    <div class="d-flex align-content-center justify-content-between w-100">
        <div class="input-group input-group-merge">
            <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span>
            <input type="text" class="form-control" id="todo-search" placeholder="Search task" aria-label="Search..."
                aria-describedby="todo-search" />
        </div>
    </div>
</div>
<!-- Todo search ends -->

@use('App\Enums\ScheduleType') @use('App\Enums\Priority')
<!-- Todo List starts -->
<div class="todo-task-list-wrapper list-group">
    <ul class="todo-task-list media-list" id="todo-task-list">
        @foreach ($tasks as $task)
            @if(Route::currentRouteName() == 'tasks.all')
            <li class="todo-item" id="todo-item-{{ $task->id }}" onclick="openCommetsModal(this)">
            @elseif (Route::currentRouteName() == 'tasks.index')
            <li class="todo-item" id="todo-item-{{ $task->id }}" onclick="openEditModal(this)">
            @elseif (Route::currentRouteName() == 'tasks.user')
            <li class="todo-item" id="todo-item-{{ $task->id }}" onclick="openStatusModal(this)">
            @endif
                <div class="todo-title-wrapper">
                    <div class="todo-title-area">
                        <div class="title-wrapper">
                            <span class="todo-title">{{ $task->title }}</span>
                        </div>
                    </div>
                    <div class="todo-item-action">
                        <div class="badge-wrapper me-1">
                            @if ($task->priority == Priority::To_Do_Last)
                                <span class="badge rounded-pill badge-light-primary">To Do
                                    Last</span>
                            @elseif ($task->priority == Priority::Normal)
                                <span class="badge rounded-pill badge-light-warning">Normal</span>
                            @elseif($task->priority == Priority::Important)
                                <span class="badge rounded-pill badge-light-danger">Important</span>
                            @endif
                        </div>
                        <small
                            class="text-nowrap text-muted me-1">{{ \Carbon\Carbon::parse($task->dead_line)->format('Y-m-d') }}</small>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="no-results">
        <h5>No Items Found</h5>
    </div>
</div>
<!-- Todo List ends -->
@if (Route::currentRouteName() == 'tasks.all') --}}
    @include('layouts.components.right_sidebar.all_tasks')
@elseif (Route::currentRouteName() == 'tasks.index')
    @include('layouts.components.right_sidebar.index')
@elseif (Route::currentRouteName() == 'tasks.user')
    @include('layouts.components.right_sidebar.user_tasks')
@endif

@use('App\Enums\ScheduleType') @use('App\Enums\Priority')
<!-- Begin Add Task -->
<div class="modal modal-slide-in sidebar-todo-modal fade" id="new-task-modal">
    <div class="modal-dialog sidebar-lg">
        <div class="modal-content p-0">
            <form id="form-modal-todo" class="todo-modal" action="{{ route('tasks.store') }}" method="POST">
                @csrf @method('POST')
                <div class="modal-header align-items-center mb-1">
                    <h5 class="modal-title">Add Task</h5>
                    <div class="todo-item-action d-flex align-items-center justify-content-between ms-auto">
                        <i data-feather="x" class="cursor-pointer" data-bs-dismiss="modal" stroke-width="3"></i>
                    </div>
                </div>
                <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                    <div class="action-tags">
                        <div class="mb-1">
                            <label for="todoTitleAdd" class="form-label">Title</label>
                            <input type="text" id="todoTitleAdd" name="title" class="new-todo-item-title form-control @error('title') is-invalid @enderror" placeholder="Title" /> @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                        </div>
                        <div class="mb-1">
                            <label for="task-due-date" class="form-label">DeadLine</label>
                            @error('dead_line')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                            <input type="text" class="form-control task-due-date @error('dead_line') is-invalid @enderror" id="task-due-date" name="dead_line" />
                        </div>
                        <div class="mb-1">
                            <label for="task-tag" class="form-label d-block">Schedule</label>
                            <select class="form-select task-tag" id="task-tag" name="schedule">
                                @foreach (ScheduleType::cases() as $type)
                                <option value="{{ $type->value }}">
                                    {{ str($type->name)->replace('_', ' ') }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="task-priority" class="form-label d-block">Priority</label>
                            <select class="form-select task-tag" id="task-tag" name="priority">
                                @foreach (Priority::cases() as $priority)
                                <option value="{{ $priority->value }}">
                                    {{ str($priority->name)->replace('_', ' ') }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="task-tag" class="form-label d-block">To User</label>
                            <select class="form-select task-tag" id="task-tag" name="user_id">
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label class="form-label">Description</label>
                            <input type="hidden" id="descriptionInput" name="description" class="@error('description') is-invalid @enderror">
                            <div id="task-desc" class="border-bottom-0" data-placeholder="Write Your Description"></div>
                            <div class="d-flex justify-content-end desc-toolbar border-top-0">
                            </div>
                            @error('description')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                        </div>
                    </div>
                    <div class="my-1">
                        <input type="submit" value="Add" class="btn btn-primary d-none add-todo-item me-1">
                        <button type="button" class="btn btn-outline-secondary add-todo-item d-none" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Add Task -->
<!-- Begin Edit Task -->
<div class="modal modal-slide-in sidebar-todo-modal fade" id="edit-task-modal">
    <div class="modal-dialog sidebar-lg">
        <div class="modal-content p-0">
            <form id="form-modal-todo-edit" class="todo-modal needs-validation" action="action=" {{ isset($task) ? route( 'tasks.update', $task->id) : '' }}" " method="POST"> @csrf @method('POST')
                <div class="modal-header align-items-center mb-1">
                    <h5 class="modal-title">Edit Task</h5>
                    <div class="todo-item-action d-flex align-items-center justify-content-between ms-auto">
                        <i data-feather="x" class="cursor-pointer" data-bs-dismiss="modal" stroke-width="3"></i>
                    </div>
                </div>
                <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                    <div class="action-tags">
                        <div class="mb-1">
                            <label for="edit-todo-title" class="form-label">Title</label>
                            <input type="text" id="edit-todo-title" name="title" class="edit-todo-item-title form-control @error('title') is-invalid @enderror" placeholder="Title" /> @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                        </div>
                        <div class="mb-1">
                            <label for="task-due-date" class="form-label">DeadLine</label>
                            <input type="text" class="form-control task-due-date @error('dead_line') is-invalid @enderror" id="task-due-date" name="dead_line" /> @error('dead_line')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                        </div>
                        <div class="mb-1">
                            <label for="task-tag" class="form-label d-block">Schedule</label>
                            <select class="form-select task-tag" id="task-tag" name="schedule">
                                @foreach (ScheduleType::cases() as $type)
                                <option value="{{ $type->value }}">{{ str($type->name)->replace('_', ' ') }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="task-priority" class="form-label d-block">Priority</label>
                            <select class="form-select task-tag" id="task-priority" name="priority">
                                @foreach (Priority::cases() as $priority)
                                <option value="{{ $priority->value }}">
                                    {{ str($priority->name)->replace('_', ' ') }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label for="task-user" class="form-label d-block">To User</label>
                            <select class="form-select task-tag" id="task-user" name="user_id">
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-1">
                            <label class="form-label">Description</label>
                            <input type="hidden" id="descriptionInput1" name="description" class="@error('description') is-invalid @enderror"> @error('description')
                            <div class="alert alert-danger">{{ $message }}</div>@enderror
                            <div id="task-desc1" class="border-bottom-0 edit-task-description" data-placeholder="Write Your Description"></div>
                            <div class="d-flex justify-content-end desc-toolbar border-top-0 ql-toolbar ql-snow">
                            </div>
                        </div>
                    </div>
                    <div class="my-1">
                        <input type="submit" value="Update" class="btn btn-primary update-todo-item me-1">
                        <a  id="delete-btn" class="btn btn-outline-danger me-1"> Delete </a>
                        <button type="button" class="btn btn-outline-secondary add-todo-item me-1" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    tasks = @json($tasks->items());
    function openEditModal(elem) {
            taskId = Number(elem.id.replace('todo-item-', ''));
            task = tasks.find(obj => obj.id == taskId);
            console.log(task);
            $("#edit-task-modal").modal('show');
            let form = $("#form-modal-todo-edit");
            url = '{{ route('tasks.update', ':id') }}'
            url = url.replace(":id", taskId);
            form.attr("action", url);
            deleteUrl = '{{ route('tasks.destroy', ':id') }}'
            deleteUrl = deleteUrl.replace(":id", taskId);
            $('#delete-btn').attr('href', deleteUrl);
            form.find('#edit-todo-title').val(task.title);
            form.find('#task-due-date').val(task.dead_line.substring(0, 10));
            form.find('#task-tag').val(task.schedule);
            form.find('#task-priority').val(task.priority);
            form.find('#task-user').val(task.user_id);
            form.find('.edit-task-description .ql-editor').text(task.description);
        }
</script>
<!-- End Edit Task -->
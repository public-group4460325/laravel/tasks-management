<!-- Right Sidebar starts -->
@if (isset($task))
    <div class="modal modal-slide-in sidebar-todo-modal fade" id="edit-task-modal">
        <div class="modal-dialog sidebar-lg">
            <div class="modal-content p-0">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"></h4>
                        <div class="d-flex">
                            <div class="author-info">
                                <small class="text-muted me-25">by</small>
                                <small class="text-body" id="leader-name"></small>
                                <br>
                                <small class="text-muted me-25">Created</small>
                                <smallt class="text-muted" id="created-at"></smallt>
                                <span class="text-muted ms-50 me-25">|</span>
                                <small class="text-muted me-25">Deadline</small>
                                <small class="text-muted" id="dead-line"></small>
                            </div>
                        </div>
                        <div class="my-1 py-25" id="schedule-priority">
                        </div>
                        <p class="card-text mb-2" id="description">
                        </p>
                        <hr class="my-2" />
                        <div class="col-xl-6 col-12" style="width: 100%">
                            <div class="card">
                                <div
                                    class="
                                card-header
                                d-flex
                                flex-sm-row flex-column
                                justify-content-md-between
                                align-items-start
                                justify-content-start
                                ">
                                </div>
                                <div class="card-body">
                                    <div id="radialbars-chart"></div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-2" />
                        <div class="my-1">
                            <span id="btn-approve"></span>
                            <button type="button" class="btn btn-outline-secondary add-todo-item me-1"
                                data-bs-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<!-- Right Sidebar ends -->
<script>
    tasks = @json($tasks->items());
    let chart;
    document.addEventListener("DOMContentLoaded", function() {
        var options = {
            chart: {
                height: 350,
                type: 'radialBar',
            },
            series: [0],
            labels: ['Progress'],
        }
        chart = new ApexCharts(document.querySelector("#radialbars-chart"), options);
        chart.render();
    });

    function openStatusModal(elem) {
        taskId = Number(elem.id.replace('todo-item-', ''));
        task = tasks.find(obj => obj.id == taskId);
        $("#card-title").html(task.title);
        $("#leader-name").html(task.leader.name);
        $("#created-at").html(task.created_at.substring(0, 10));
        $("#dead-line").html(task.dead_line.substring(0, 10));
        let html = `<span class="badge rounded-pill badge-light-`;
        if (task.priority == 0)
            html += `primary">To Do Last </span>`;
        else if (task.priority == 1) {
            html += `warning">Normal </span>`;
        } else {
            html += `danger">Important </span>`;
        }
        html += `<span class="badge rounded-pill badge-light-`
        if (task.schedule == 0)
            html += `success">None </span>`;
        else if (task.schedule == 1) {
            html += `danger">Daily </span>`;
        } else if (task.schedule == 2) {
            html += `warning">Weekly </span>`;
        } else {
            html += `primary">Monthly </span>`;
        }
        let btn;
        let url = '{{ route('tasks.approveAndComplete', ':id') }}'
        url = url.replace(":id", task.id);
        if (task.status == 0) {
            btn = `<a href="` + url + `"class="btn btn-outline-primary update-todo-item me-1"> Approve </a>`;
        } else if (task.status == 1) {
            btn = `<a href="` + url + `"class="btn btn-outline-primary update-todo-item me-1"> Mark as completed </a>`;
        } else {
            btn = `<a class="btn btn-outline-success update-todo-item me-1"> completed </a>`;
        }
        $("#btn-approve").html(btn);
        $("#schedule-priority").html(html);
        $("#description").html(task.description);
        let series;
        if (task.status == 2) {
            series = [100];
        } else if (task.status == 1) {
            series = [67];
        } else {
            series = [33]
        }
        chart.updateSeries(series)
        $("#edit-task-modal").modal('show');
    }
</script>

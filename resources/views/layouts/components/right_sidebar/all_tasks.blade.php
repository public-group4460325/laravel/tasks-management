<!-- Right Sidebar starts -->
@if (isset($task))
    <div class="modal modal-slide-in sidebar-todo-modal fade" id="comment-modal">
        <div class="modal-dialog sidebar-lg">
            <div class="modal-content p-0">
                <form id="form-modal-todo-edit" class="todo-modal needs-validation"
                    action=" {{ isset($task) ? route('comment.store', $task->id) : '' }}" method="POST"
                    enctype="multipart/form-data"> @csrf @method('POST')
                    <div class="modal-header align-items-center mb-1">
                        <h5 class="modal-title">Edit Task</h5>
                        <div class="todo-item-action d-flex align-items-center justify-content-between ms-auto">
                            <i data-feather="x" class="cursor-pointer" data-bs-dismiss="modal" stroke-width="3"></i>
                        </div>
                    </div>
                    <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                        <div class="action-tags">
                            <div class="mb-1">
                                <label for="formFile" class="form-label">Attachment</label>
                                <input name="file" class="form-control " type="file" id="formFile">
                            </div>
                            <div class="mb-1">
                                <label class="form-label">Comment</label>
                                <input type="hidden" id="descriptionInput1" name="comment"
                                    class="@error('description') is-invalid @enderror">
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div id="task-desc1" class="border-bottom-0 edit-task-description"
                                    data-placeholder="Write A Comment"></div>
                                <div class="d-flex justify-content-end desc-toolbar border-top-0 ql-toolbar ql-snow">
                                </div>
                            </div>
                        </div>
                        <div class="my-1">
                            <input type="submit" value="Send" class="btn btn-primary update-todo-item me-1">
                            <button type="button" class="btn btn-outline-secondary add-todo-item me-1"
                                data-bs-dismiss="modal">Back</button>
                        </div>
                        <div class="my-1">
                            <div class="todo-task-list-wrapper list-group">
                                <ul class="todo-task-list media-list" id="todo-task-list">
                                    @foreach ($task->comments as $comment)
                                        <li class="todo-item" id="todo-item">
                                            <div class="todo-title-wrapper">
                                                <div class="todo-title-area">
                                                    @if ($comment->attachment != null)
                                                        <a href="{{ route('comment.download', $comment->id) }}"><i
                                                                data-feather="paperclip"></i></a>
                                                    @endif
                                                    <div class="title-wrapper">
                                                        <span class="todo-title"
                                                            style="word-wrap: break-word; width: 250px">{{ $comment->comment }}</span>
                                                    </div>
                                                </div>
                                                <div class="todo-item-action">
                                                    <div class="badge-wrapper me-1">
                                                    </div>
                                                    <small
                                                        class="text-nowrap text-muted me-1">{{ $comment->user->name }}</small>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="no-results">
                                    <h5>No Items Found</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        tasks = @json($tasks->items());
        function openCommetsModal(elem) {
            test = document.getElementById('comment-modal');
            console.log(test);
            taskId = Number(elem.id.replace('todo-item-', ''));
            task = tasks.find(obj => obj.id == taskId);
            $("#comment-modal").modal('show');
        }
    </script>
@endif
<!-- Right Sidebar ends -->

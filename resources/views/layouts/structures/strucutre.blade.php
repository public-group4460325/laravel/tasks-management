<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description"
        content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>IxCoders</title>
    @include('layouts.structures.css_links')
</head>

<body class="vertical-layout vertical-menu-modern content-left-sidebar navbar-floating footer-static menu-collapsed"
    data-open="click" data-menu="vertical-menu-modern" data-col="content-left-sidebar">
    @include('layouts.structures.sidebar')

    <div class="app-content content todo-application">
        <div class="content-area-wrapper container-xxl p-0">
            @yield('content')
        </div>
    </div>
    @include('layouts.structures.js_links')

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>

</html>

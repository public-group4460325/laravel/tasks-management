<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto">
                <a class="navbar-brand" href="#">
                    <h2 class="brand-text">IxCoders</h2>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('profile.show') }}"><i
                        data-feather="user"></i><span class="menu-title text-truncate"
                        data-i18n="Email">Profile</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('tasks.index') }}"><i
                        data-feather="check"></i><span class="menu-title text-truncate" data-i18n="Email">My Leading
                        Tasks</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('tasks.user') }}"><i
                        data-feather="check-square"></i><span class="menu-title text-truncate" data-i18n="Email">My
                        Tasks</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('tasks.all') }}"><i
                        data-feather="list"></i><span class="menu-title text-truncate" data-i18n="Email">All
                        Tasks</span></a>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('logout') }}"><i
                        data-feather="log-out"></i><span class="menu-title text-truncate"
                        data-i18n="Email">Logout</span></a>
            </li>
        </ul>
    </div>
</div>
<!-- END: Main Menu-->

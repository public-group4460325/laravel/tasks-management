<div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <form id="checkCodeForm" class="mt-2"
            action="{{ route('checkVerificationCode',Route::getRoutes()->match(Request::create(url()->previous()))->getName()) }}"
            method="POST">
            @csrf @method('POST')
            <h6>Type your 6 digit security code</h6>
            <div class="auth-input-wrapper d-flex align-items-center justify-content-between">
                <input id="code-box-1" oninput="goNext(this)"
                    class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1  @error('verification_code.0') is-invalid @enderror"
                    type="text" name="verification_code[]" maxlength="1" autofocus="" />
                <input id="code-box-2" oninput="goNext(this)"
                    class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1  @error('verification_code.1') is-invalid @enderror"
                    type="text" name="verification_code[]" maxlength="1" />
                <input id="code-box-3" oninput="goNext(this)"
                    class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1  @error('verification_code.2') is-invalid @enderror"
                    type="text" name="verification_code[]" maxlength="1" />
                <input id="code-box-4" oninput="goNext(this)"
                    class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1  @error('verification_code.3') is-invalid @enderror"
                    type="text" name="verification_code[]" maxlength="1" />
                <input id="code-box-5" oninput="goNext(this)"
                    class="form-control auth-input height-50 text-center numeral-mask mx-25 mb-1  @error('verification_code.4') is-invalid @enderror"
                    type="text" name="verification_code[]" maxlength="1" />
            </div>
            @if (session()->has('error'))
                <div class="alert alert-danger"> {{ session('error') }} </div>
            @endif
            <input type="submit" value="Verify my account" class="btn btn-primary w-100">
        </form>
    </div>
</div>

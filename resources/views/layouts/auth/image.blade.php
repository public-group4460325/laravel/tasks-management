<a class="brand-logo" href="index.html">
    <h2 class="brand-text text-primary ms-1">IxCoders</h2>
</a>
<div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
    <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
        <img class="img-fluid" src="{{ asset($imagePath) }}" alt="Image" />
    </div>
</div>


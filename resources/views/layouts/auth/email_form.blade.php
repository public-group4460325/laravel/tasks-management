<div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title fw-bold mb-1">Enter Email 🔒</h2>
        <p class="card-text mb-2">Please enter your email to recieve verification code</p>
        <form class="auth-reset-password-form mt-2" action="{{ route('checkEmail') }}" method="POST">
            @csrf @method('POST')
            <div class="mb-1">
                <div class="mb-1">
                    <label class="form-label" for="login-email">Email</label>
                    <input class="form-control @error('email') is-invalid @enderror" id="login-email" type="text"
                        name="email" placeholder="example@example.com" aria-describedby="login-email" autofocus=""
                        tabindex="1" />
                </div>
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror @if (session()->has('error'))
                    <div class="alert alert-danger"> {{ session('error') }} </div>
                @endif
                <input type="submit" value="Check Email" class="btn btn-primary w-100" tabindex="3">
            </form>
        </div>
    </div>

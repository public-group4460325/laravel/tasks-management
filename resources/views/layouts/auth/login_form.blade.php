<div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
        <h2 class="card-title fw-bold mb-1">Welcome to IxCoders! 👋</h2>
        <p class="card-text mb-2">Please sign-in to your account and start the adventure</p>
        <form class="auth-login-form mt-2" action="{{ route('signIn') }}" method="POST">
            @csrf @method('POST')
            <div class="mb-1">
                <label class="form-label" for="login-email">Email</label>
                <input class="form-control @error('email') is-invalid @enderror" id="login-email" type="text"
                    name="email" placeholder="example@example.com" aria-describedby="login-email" autofocus=""
                    tabindex="1" />
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="mb-1">
                <div class="d-flex justify-content-between">
                    <label class="form-label" for="login-password">Password</label><a
                        href="{{ route('forgetPassword') }}"><small>Forgot Password?</small></a>
                </div>
                <div class="input-group input-group-merge form-password-toggle">
                    <input class="form-control form-control-merge @error('password') is-invalid @enderror"
                        id="login-password" type="password" name="password" placeholder="············"
                        aria-describedby="login-password" tabindex="2" /><span
                        class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                </div>
            </div>
            @error('password')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror @if (session()->has('error'))
                    <div class="alert alert-danger"> {{ session('error') }} </div>
                @endif
                <input type="submit" value="Login" class="btn btn-primary w-100" tabindex="4">
            </form>
            <p class="text-center mt-2"><span>New on our platform?</span><a
                    href="{{ route('register') }}"><span>&nbsp;Create an account</span></a></p>
        </div>
    </div>

@extends('layouts.structures.strucutre') @section('content')
    <!-- BEGIN: Content-->
    @include('layouts.components.left_sidebar')
    <div class="content-right">
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="body-content-overlay"></div>
                <div class="todo-app-list">
                    @include('layouts.components.todo_search')
                    @include('layouts.components.tasks_list')
                </div>
                
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection

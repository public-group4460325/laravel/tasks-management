<?php

namespace App\Observers;

use App\Models\Task;
use Illuminate\Support\Facades\Log;

class TaskObserver
{
    /**
     * Handle the Comment "created" event.
     */
    public function created(Task $task): void
    {
        Log::info('Task Created: ', $task->toArray());
    }

    /**
     * Handle the Comment "updated" event.
     */
    public function updated(Task $task): void
    {
        Log::info('Task Updated: ', $task->toArray());
    }

    /**
     * Handle the Comment "deleted" event.
     */
    public function deleted(Task $task): void
    {
        Log::info('Task Deleted: ', $task->toArray());
    }

    /**
     * Handle the Comment "restored" event.
     */
    public function restored(Task $task): void
    {
        Log::info('Task Restored: ', $task->toArray());
    }

    /**
     * Handle the Comment "force deleted" event.
     */
    public function forceDeleted(Task $task): void
    {
        Log::info('Task ForceDeleted: ', $task->toArray());
    }
}

<?php

namespace App\Services;

use App\Enums\Priority;
use App\Enums\ScheduleType;
use App\Enums\TaskStatus;
use App\Jobs\SendAssignment;
use App\Jobs\SendDeadLine;
use App\Repositories\TaskRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class TaskService
{
    protected $taskRepository;
    protected $userRepository;

    public function __construct(TaskRepository $taskRepository, UserRepository $userRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->userRepository = $userRepository;
    }

    public function getTasksWithUsers()
    {
        $user = Auth::user();
        $tasks = $this->taskRepository->getTasksForUser($user->id);
        $users = $this->userRepository->getAllUsers();
        session()->remove('user');
        return compact('tasks', 'users');
    }

    public function storeTask($request)
    {
        $user = Auth::user();
        $data = $request->validated();
        $data['priority'] = Priority::matchEnum($request->priority);
        $data['schedule'] = ScheduleType::matchEnum($request->schedule);
        $data['leader_id'] = $user->id;

        $task = $this->taskRepository->createTask($data);

        $delayInSeconds = strtotime($task->dead_line) - time();
        SendDeadLine::dispatch($task->id)->delay($delayInSeconds);
        SendAssignment::dispatch($task->id);
    }

    public function updateTask($id, $request)
    {
        $task = $this->taskRepository->findTaskById($id);

        $data = $request->validated();
        $data['priority'] = Priority::matchEnum($request->priority);
        $data['schedule'] = ScheduleType::matchEnum($request->schedule);

        if ($task->user_id != $data['user_id']) {
            SendAssignment::dispatch($task->id);
        }

        $this->taskRepository->updateTask($task, $data);
    }

    public function deleteTask($id)
    {
        $task = $this->taskRepository->findTaskById($id);
        $this->taskRepository->deleteTask($task);
    }

    public function approveAndCompleteTask($id)
    {
        $task = $this->taskRepository->findTaskById($id);
        $data = ['status' => 0];
        if ($task->status == TaskStatus::Done) return;

        if ($task->status == TaskStatus::To_Do) {
            $data['status'] = 1; // Assuming 1 is the status for 'In Progress'
        } elseif ($task->status == TaskStatus::In_Progress) {
            $data['status'] = 2; // Assuming 2 is the status for 'Done'
        }

        $this->taskRepository->updateTask($task, $data);
    }

    public function getAllTasks()
    {
        return $this->taskRepository->getAllTasks();
    }

    public function getUserTasks($page)
    {
        return Cache::remember("my_tasks_page_$page", now()->addHour(), function () use ($page) {
            return $this->taskRepository->getTasksByUserId(auth()->id(), $page);
        });
    }
}

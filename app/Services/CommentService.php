<?php

namespace App\Services;

use App\Http\Requests\comment\StoreRequest;
use App\Repositories\CommentRepository;
use Illuminate\Support\Facades\Storage;

class CommentService
{
    protected $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function storeComment(StoreRequest $request, string $task_id)
    {
        $user = auth()->user();
        $data = $request->validated();

        if ($request->has('file')) {
            $data['attachment'] = $request->file('file')->store('comments/' . $task_id, 'public');
        }

        $data['user_id'] = $user->id;
        $data['task_id'] = $task_id;

        $this->commentRepository->createComment($data);
    }

    public function downloadAttachment(string $id)
    {
        $comment = $this->commentRepository->findCommentById($id);
        $filePath = 'public/' . $comment->attachment;

        if (!Storage::exists($filePath)) {
            abort(404);
        }

        return response()->download(storage_path('app/' . $filePath));
    }
}

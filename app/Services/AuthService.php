<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Traits\SendEmailTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    use SendEmailTrait;

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function signup($request)
    {
        $user = $this->userRepository->findByEmail($request->email);
        $verificationCode = rand(10000, 99999);
        $data = $request->validated();
        $data['verification_code'] = $verificationCode;
        $user = $this->userRepository->create($data);
        session()->put('user', $user);
        $this->sendVerifyEmail($request->email, $request->name, $verificationCode);
    }

    public function checkVerificationCode($request, $preRoute)
    {
        $verification_code = implode('', $request->verification_code);
        if (!session()->has('user')) return;
        
        $user = auth()->check() ? auth()->user() : session()->get('user');

        if ($user->verification_code != $verification_code) {
            return back()->with('error', 'Verification Code Not Match');
        }

        $user->email_verified_at = Carbon::now();
        $user->save();

        if ($preRoute == 'forgetPassword') {
            return redirect()->route('resetPassword');
        } elseif ($preRoute == 'register' || $preRoute == 'login') {
            Auth::login($user);
            return redirect()->route('tasks.index');
        } elseif ($preRoute == 'profile.show') {
            $data = session()->get('updateProfile');
            $user->update($data);
            session()->remove('updateProfile');
            return redirect()->route('tasks.index');
        }
    }

    public function signIn($request)
    {
        return Auth::attempt(['email' => $request->email, 'password' => $request->password]);
    }

    public function checkEmail($request)
    {
        $user = $this->userRepository->findByEmail($request->email);
        if (!$user) return back()->with('error', 'Email Not Found');

        session()->put('user', $user);
        $verificationCode = rand(10000, 99999);
        $user->verification_code = $verificationCode;
        $user->email_verified_at = null;
        $user->save();
        $this->sendVerifyEmail($request->email, $request->name, $verificationCode);
    }

    public function updatePassword($request)
    {
        if (!session()->has('user')) return;

        $user = session()->get('user');
        $user->password = $request->password;
        $user->save();
        Auth::login($user);
    }

    public function logout()
    {
        session()->flush();
        Auth::logout();
    }

    public function profileUpdate($request)
    {
        $user = auth()->user();
        if ($user->email != $request->email) {
            $verificationCode = rand(10000, 99999);
            $user->verification_code = $verificationCode;
            $user->save();
            $this->sendVerifyEmail($request->email, $request->name, $verificationCode);
            session()->put('updateProfile', $request->validated());
            return redirect()->route('verificationCode');
        } else {
            $data = $request->validated();
            $user->update($data);
        }
    }
}

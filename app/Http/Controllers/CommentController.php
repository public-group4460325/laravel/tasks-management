<?php

namespace App\Http\Controllers;

use App\Http\Requests\comment\StoreRequest;
use App\Services\CommentService;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    protected $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function store(StoreRequest $request, string $task_id)
    {
        $this->commentService->storeComment($request, $task_id);
        return back();
    }

    public function download(string $id)
    {
        return $this->commentService->downloadAttachment($id);
    }
}

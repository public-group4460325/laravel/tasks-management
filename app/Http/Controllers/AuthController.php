<?php

namespace App\Http\Controllers;

use App\Http\Requests\auth\checkEmailRequest;
use App\Http\Requests\auth\CheckVerificationCodeRequest;
use App\Http\Requests\auth\ProfileUpdateRequest;
use App\Http\Requests\auth\SignInRequest;
use App\Http\Requests\auth\SignUpRequest;
use App\Providers\AuthServiceProvider;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register()
    {
        return view('auth.register');
    }

    public function signup(SignUpRequest $request)
    {
        $this->authService->signup($request);
        return redirect()->route('verificationCode');
    }

    public function checkVerificationCode(CheckVerificationCodeRequest $request, string $preRoute)
    {
        $this->authService->checkVerificationCode($request, $preRoute);
        return redirect()->route('tasks.index');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function signIn(SignInRequest $request)
    {
        if (!$this->authService->signIn($request)) {
            return back()->with('error', 'Information Not Match');
        }
        return redirect()->route('tasks.index');
    }

    public function verificationCode()
    {
        return view('auth.verificationCode');
    }

    public function forgetPassword()
    {
        return view('auth.checkEmail');
    }

    public function checkEmail(checkEmailRequest $request)
    {
        $this->authService->checkEmail($request);
        return redirect()->route('verificationCode');
    }

    public function resetPassword()
    {
        return view('auth.resetPassword');
    }

    public function updatePassword(Request $request)
    {
        $this->authService->updatePassword($request);
        return redirect()->route('tasks.index');
    }

    public function logout()
    {
        $this->authService->logout();
        return redirect()->route('login');
    }

    public function profile()
    {
        $user = auth()->user();
        return view('auth.profile', compact('user'));
    }

    public function profileUpdate(ProfileUpdateRequest $request)
    {
        $this->authService->profileUpdate($request);
        return redirect()->route('tasks.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\comment\StoreRequest;
use App\Http\Requests\tasks\UpdateRequest;
use App\Services\TaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function index()
    {
        $data = $this->taskService->getTasksWithUsers();
        return view('tasks.index', $data);
    }

    public function userTasks()
    {
        $page = request()->query('page');
        $tasks = $this->taskService->getUserTasks($page);
        return view('tasks.user_tasks', compact('tasks'));
    }

    public function store(StoreRequest $request)
    {
        $this->taskService->storeTask($request);
        return redirect()->route('tasks.index');
    }

    public function update(UpdateRequest $request, string $id)
    {
        $this->taskService->updateTask($id, $request);
        return redirect()->route('tasks.index');
    }

    public function destroy(string $id)
    {
        $this->taskService->deleteTask($id);
        return redirect()->route('tasks.index');
    }

    public function approveAndComplete(string $id)
    {
        $this->taskService->approveAndCompleteTask($id);
        return redirect()->route('tasks.user');
    }

    public function all()
    {
        $tasks = $this->taskService->getAllTasks();
        return view('tasks.all_tasks', compact('tasks'));
    }
}

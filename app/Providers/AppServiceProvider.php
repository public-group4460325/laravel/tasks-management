<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        \App\Models\Comment::observe(\App\Observers\CommentObserver::class);
        \App\Models\Task::observe(\App\Observers\TaskObserver::class);
        \App\Models\User::observe(\App\Observers\UserObserver::class);
    }

    public function commands($commands)
    {
        \App\Console\Commands\MakeLogObservers::class;
        \App\Console\Commands\MakeLogObserver::class;
        \App\Console\Commands\MakeControllerServiceRepository::class;
    }
}

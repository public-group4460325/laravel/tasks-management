<?php

namespace App\Repositories;

use App\Models\Task;

class TaskRepository
{
    public function getTasksForUser($leaderId)
    {
        return Task::where('leader_id', $leaderId)
            ->orderBy('priority', 'desc')
            ->paginate(15);
    }

    public function createTask(array $data)
    {
        return Task::create($data);
    }

    public function updateTask(Task $task, array $data)
    {
        $task->update($data);
    }

    public function findTaskById($id)
    {
        return Task::findOrFail($id);
    }

    public function deleteTask(Task $task)
    {
        $task->delete();
    }

    public function getAllTasks()
    {
        return Task::orderBy('priority', 'desc')->paginate(15);
    }
    
    public function getTasksByUserId($userId, $page)
    {
        return Task::with('leader')
            ->where('user_id', $userId)
            ->orderBy('priority', 'desc')
            ->paginate(15, ['*'], 'page', $page);
    }

}

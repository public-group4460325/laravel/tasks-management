<?php

namespace App\Repositories;

use App\Models\Comment;

class CommentRepository
{
    public function createComment(array $data)
    {
        return Comment::create($data);
    }

    public function findCommentById(string $id)
    {
        return Comment::findOrFail($id);
    }
}

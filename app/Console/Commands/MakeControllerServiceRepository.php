<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MakeControllerServiceRepository extends Command
{
    protected $signature = 'make:csr {fullPath}';
    protected $description = 'Create a new Controller, Service, and Repository with a given path and model';

    public function handle()
    {
        $fullPath = $this->argument('fullPath');
        $path = dirname($fullPath); 
        $model = basename($fullPath);
        $controller = $model . 'Controller';
        $service = $model . 'Service';
        $repository = $model . 'Repository';
        $modelCamel = Str::camel($model);

        $this->createDirectoryIfNotExists(app_path('Http/Controllers/' . $path));
        $this->createDirectoryIfNotExists(app_path('Services/' . $path));
        $this->createDirectoryIfNotExists(app_path('Repositories/' . $path));

        $this->createFile('controller.stub', 'Http/Controllers/' . $path . '/' . $controller . '.php', [
            'path' => str_replace('/', '\\', $path),
            'controller' => $controller,
            'service' => $service,
            'serviceVariable' => Str::camel($service),
            'viewPath' => Str::kebab($model)
        ]);

        $this->createFile('service.stub', 'Services/' . $path . '/' . $service . '.php', [
            'path' => str_replace('/', '\\', $path),
            'service' => $service,
            'repository' => $repository,
            'repositoryVariable' => Str::camel($repository)
        ]);

        $this->createFile('repository.stub', 'Repositories/' . $path . '/' . $repository . '.php', [
            'path' => str_replace('/', '\\', $path),
            'repository' => $repository,
            'model' => $model
        ]);

        $this->info('Controller, Service, and Repository created successfully.');
    }

    protected function createFile($stub, $path, $replace = [])
    {
        $stubPath = resource_path('stubs/' . $stub);
        $content = File::get($stubPath);

        foreach ($replace as $search => $replaceValue) {
            $content = str_replace('{{ ' . $search . ' }}', $replaceValue, $content);
        }

        File::put(app_path($path), $content);
    }

    protected function createDirectoryIfNotExists($path)
    {
        if (!File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }
    }
}
